//
//  ViewController.swift
//  Pick Up Lines Proto
//
//  Created by Kyle Lee on 10/14/17.
//  Copyright © 2017 Kilo Loco. All rights reserved.
//

import UIKit
import RealmSwift

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var pickUpLines: Results<PickUpLine>!
    var notificationToken: NotificationToken?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let realm = RealmService.shared.realm
//        self.pickUpLines = realm.objects(PickUpLine.self)
        
        self.pickUpLines = RealmService.shared.read(PickUpLine.self)
        
        // when something changes in realm file, tableview will reload data
        notificationToken = RealmService.shared.realm.observe { [unowned self] (notification, realm) in
            print(realm)
            self.tableView.reloadData()
        }
        
        RealmService.shared.observeRealmErrors(in: self) { (error) in
            print(error ?? "no error detected")
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        notificationToken?.invalidate() // stop the notification
        RealmService.shared.stopObservingErrors(in: self)
    }

    @IBAction func onAddTapped() {
        AlertService.addAlert(in: self) { (line, score, email) in
            let newPickUpLine = PickUpLine(line: line, score: score, email: email)
            RealmService.shared.create(newPickUpLine)
        }
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.pickUpLines.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PickUpLineCell") as? PickUpLineCell else { return UITableViewCell() }
        let pickUpLine = pickUpLines[indexPath.row]
        cell.configure(with: pickUpLine)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 63
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let pickUpLine = pickUpLines[indexPath.row]
        AlertService.updateAlert(in: self, pickUpLine: pickUpLine) { (line, score, email) in
            let dict: [String: Any?] = ["line": line, "score": score, "email": email]
            RealmService.shared.update(pickUpLine, with: dict)
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        guard editingStyle == .delete else { return }
        let pickUpLine = pickUpLines[indexPath.row]
        RealmService.shared.delete(pickUpLine)
    }
}
















